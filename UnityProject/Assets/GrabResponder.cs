﻿using UnityEngine;
using System.Collections;

//A Simple example of Implementation
[AddComponentMenu ("CustomScripts/TriggerSystem/Responders/GrabResponder")]
public class GrabResponder : ObjectTriggerResponder {

	public GameObject Target;
	bool active = false;

	public override void OnTrigger()
	{
		active = !active;
		if (active)
			transform.parent = Target.gameObject.transform;
		else
			transform.parent = null;
	}
}

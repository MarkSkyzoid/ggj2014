﻿using UnityEngine;
using System.Collections;

[AddComponentMenu ("CustomScripts/PlayerStuff/PlayerBehaviour")]
public class PlayerBehaviour : MonoBehaviour {
	
	SaveGameData saveFile = new SaveGameData();
	// Use this for Initialization
	void Start () {
		SetUp ();

	}

	void SetUp()
	{
		SaveLoad.Load (ref saveFile);
		transform.position = new Vector3 (saveFile.PositionX, saveFile.PositionY, 0);
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 pos = transform.position;
		if (pos.y < -10) {
			TakeDamage();
		}
		if (Input.GetButtonDown ("Fire1")) {
			saveFile.Initialise(pos.x, pos.y, Application.loadedLevelName);
			SaveLoad.Save(saveFile);
		}
	}

	public void TakeDamage()
	{
		Respawn ();
	}

	void Respawn()
	{
		Application.Quit();
		Application.LoadLevel(Application.loadedLevelName);
	}

}

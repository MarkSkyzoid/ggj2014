﻿using UnityEngine;
using System.Collections;

[AddComponentMenu ("CustomScripts/AI/Patroller")]
public class Patroller : AI {

	public float Radius, Speed;
	private int NextNode = 0;

	public GameObject[] Nodes;
	
	
	protected override void IdleCode()
	{
		NextState = AIStates.PATROLLING;
	}
	protected override void FollowCode(){}
	protected override void PatrolCode()
	{
		float DeltaTime = Time.deltaTime;
		if (Nodes.Length > 0) 
		{
			transform.position += (Nodes[Mathf.Abs(NextNode)].transform.position - transform.position).normalized * Speed * DeltaTime;
			float Distance = Vector3.Distance(transform.position, Nodes[Mathf.Abs(NextNode)].transform.position);
			if( Distance < (Speed * DeltaTime)) 
				NextNode++;

			if(NextNode == Nodes.Length)
				NextNode = (-NextNode) + 1;
		}

		if(TargetInRange())
			NextState = AIStates.ATTACKING;
	}
	protected override void AttackCode()
	{
		float DeltaTime = Time.deltaTime;
		transform.position +=  (Target.transform.position - transform.position).normalized * Speed * DeltaTime;
		
		if (!TargetInRange ())
			NextState = AIStates.IDLE;
	}
	
	bool TargetInRange()
	{
		if(Vector2.Distance(Target.transform.position, transform.position) < Radius)
			return true;
		return false;
	}
	
	void OnCollisionEnter2D(Collision2D collider)
	{
		if(Target == collider.collider.gameObject)
		{
			if(Target.tag == "Player")
				Target.GetComponent<PlayerBehaviour>().TakeDamage ();
		}
	}
	
	public override void HasPulsed()
	{
		if (CurrentState == AIStates.FOLLOWING)
			NextState = AIStates.ATTACKING;
	}
}

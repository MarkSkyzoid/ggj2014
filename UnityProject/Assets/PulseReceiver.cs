﻿using UnityEngine;
using System.Collections;

public class PulseReceiver : MonoBehaviour
{
    public Component[] pulseControlledComponants;
    public bool activeByDefault = true;
    public bool pulseHit = false;

	void Start () 
    {
        //If object shouldn't be active by default, deactivate
        if (!activeByDefault)
        {
            //gameObject.collider.isTrigger = !activeByDefault;
            //gameObject.renderer.enabled = activeByDefault;
            foreach (Component com in pulseControlledComponants)
            {
                Collider2D col = com as Collider2D;
                if (col)
                    col.isTrigger = !activeByDefault;

                Renderer ren = com as Renderer;
                if (ren)
                    ren.enabled = activeByDefault;

                Rigidbody2D rb = com as Rigidbody2D;
                if (rb)
                    rb.isKinematic = !activeByDefault;

                MonoBehaviour mon = com as MonoBehaviour;
                if (mon)
                    mon.enabled = activeByDefault;
            }
        }
	}
	
	void Update () 
    {
        if (pulseHit && !PulseController.pulsing)
            PulseExit();
	}

    public void PulseEnter()
    {
        pulseHit = true;
        //gameObject.collider.isTrigger = activeByDefault;
        //gameObject.renderer.enabled = !activeByDefault;
        foreach (Component com in pulseControlledComponants)
        {
            Collider2D col = com as Collider2D;
            if (col)
                col.isTrigger = activeByDefault;

            Renderer ren = com as Renderer;
            if (ren)
                ren.enabled = !activeByDefault;

            Rigidbody2D rb = com as Rigidbody2D;
            if (rb)
                rb.isKinematic = activeByDefault;

            MonoBehaviour mon = com as MonoBehaviour;
            if (mon)
                mon.enabled = !activeByDefault;
        }
    }

    public void PulseExit()
    {
        pulseHit = false;
        //gameObject.collider.isTrigger = !activeByDefault;
        //gameObject.renderer.enabled = activeByDefault;
        foreach (Component com in pulseControlledComponants)
        {
            Collider2D col = com as Collider2D;
            if (col)
                col.isTrigger = !activeByDefault;

            Renderer ren = com as Renderer;
            if (ren)
                ren.enabled = activeByDefault;

            Rigidbody2D rb = com as Rigidbody2D;
            if (rb)
                rb.isKinematic = !activeByDefault;

            MonoBehaviour mon = com as MonoBehaviour;
            if (mon)
                mon.enabled = activeByDefault;
        }
    }
}

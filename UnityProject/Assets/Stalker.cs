﻿using UnityEngine;
using System.Collections;

[AddComponentMenu ("CustomScripts/AI/Stalker")]
public class Stalker : AI {

	
	public float Radius, InnerRadius, Speed;
	
	
	protected override void IdleCode()
	{
		if(TargetInRange())
			NextState = AIStates.FOLLOWING;
	}
	protected override void FollowCode()
	{
		float DeltaTime = Time.deltaTime;
		Vector3 PosOffset = (Target.transform.position - transform.position).normalized * Speed * DeltaTime;

		if(Vector2.Distance(Target.transform.position, transform.position + PosOffset) < InnerRadius)
			return;
		else
			transform.position += PosOffset;
	}
	protected override void PatrolCode(){}
	protected override void AttackCode()
	{
		float DeltaTime = Time.deltaTime;
		transform.position +=  (Target.transform.position - transform.position).normalized * Speed * DeltaTime;
		
		if (!TargetInRange ())
			NextState = AIStates.IDLE;
	}
	
	bool TargetInRange()
	{
		if(Vector2.Distance(Target.transform.position, transform.position) < Radius)
			return true;
		return false;
	}
	
	void OnCollisionEnter2D(Collision2D collider)
	{
		if(Target == collider.collider.gameObject)
		{
			if(Target.tag == "Player")
				Target.GetComponent<PlayerBehaviour>().TakeDamage ();
		}
	}
	
	public override void HasPulsed()
	{
		if (CurrentState == AIStates.FOLLOWING)
			NextState = AIStates.ATTACKING;
	}
}


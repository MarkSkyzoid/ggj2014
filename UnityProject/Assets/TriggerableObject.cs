﻿using UnityEngine;
using System;
using System.Collections;

public class TriggerableObject : MonoBehaviour {

	protected short INVALID_IDX = -1;

    public const int NumTargets = 15;

    public GameObject[] Targets = new GameObject[NumTargets];
    public String[] TargetNames = new String[NumTargets];

    protected int FindTargetByName(String name)
    {
    	for(int i = 0; i < NumTargets; ++i)
    	{
    		if(TargetNames[i].Equals(name))
    		{
    			return i;	
    		}
    	}

    	return INVALID_IDX;
    }

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}

    // Override the following events to trigger specific actions
    public virtual void OnCollisionEnter2D(Collision2D coll) {

    }

    public virtual void OnCollisionExit2D(Collision2D coll)
    {

    }

    public virtual void OnCollisionStay2D(Collision2D coll)
    {

    }
}

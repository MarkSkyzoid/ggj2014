
using UnityEngine;

[AddComponentMenu ("CustomScripts/Environment/KillBox")]
public class KillBox : MonoBehaviour
{
	void OnCollisionEnter2D(Collision2D collision)
	{
		GameObject CollidedObject = collision.collider.gameObject;
		if (CollidedObject.tag == "Player")
		{
			CollidedObject.GetComponent<PlayerBehaviour>().TakeDamage();
		}

	}
}

﻿Shader "Custom/RevealingCircle" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_TimePassed ("TimePassed", Float) = 0.0
	}
	SubShader {
		Tags 
		{ 
		   "Queue"="Transparent" 
		   "IgnoreProjector"="False" 
		   "RenderType"="Transparent" 
		   "PreviewType"="Plane"
		   "CanUseSpriteAtlas"="True"
		}

		Cull off
		ZWrite off
		Lighting off
	    Fog { Mode Off }
	    Blend SrcAlpha OneMinusSrcAlpha
		
		CGPROGRAM
		#pragma surface surf Lambert

		sampler2D _MainTex;
		float _TimePassed;

		struct Input {
			float2 uv_MainTex;
		};

		void surf (Input IN, inout SurfaceOutput o) {
			float2 uv = IN.uv_MainTex;

			uv -= float2(0.5, 0.5);
			float dist = uv.x * uv.x + uv.y * uv.y; 
			
			float modifier = 0.5 * (1 + sin(_TimePassed )) + 0.7;

			half4 c = half4(0,0,0,0); 
			if(dist <= 0.08 * modifier && dist >= 0.05 * modifier)
			{
				float t = dist - 0.05 * modifier;
				t /= (0.08 - 0.05) * (modifier < 0.000001 ? 0.000001 : modifier);

				c = half4(1,1,1, smoothstep(0.8, 0.0, t));

				//c = half4(1,1,1,1);
			}
			else
			{
				half4(0,0,0,0);
			}
			//tex2D (_MainTex, uv);
			o.Albedo = c.rgb;
			o.Alpha = c.a;		}
		ENDCG
	} 
	FallBack "Diffuse"
}

﻿using UnityEngine;
using System.Collections;

public class RevealingCircleScript : MonoBehaviour {

	public bool Active = false;
	public float Size = 5.0f;

	public float Duration = 5.0f;

	float time = 0.0f;

	SpriteRenderer sprRnd = null;

	// Use this for initialization
	void Start () {
		time = 0.0f;
		Active = false;
		sprRnd = gameObject.GetComponent<SpriteRenderer>();
		if(sprRnd != null)
		{
			sprRnd.enabled = false;
		}
	}
	
	// Update is called once per frame
	void Update () {

		if(Active)
		{
			if(sprRnd != null && !sprRnd.enabled)
			{
				sprRnd.enabled = true;
			}

			UpdateImpl();
		}
	}

	public void Activate()
	{
		if(Active || sprRnd == null)
			return;

		Debug.Log("Activating!");

		time = 0.0f;
		Active = true;
		sprRnd.material.SetFloat("_TimePassed", time);
		gameObject.transform.localScale = new Vector3(1,1,1);
	}

	private void UpdateImpl()
	{
		time += Time.fixedDeltaTime;

		if(sprRnd != null)
		{	
			sprRnd.material.SetFloat("_TimePassed", time);
		}

		float modifier = (0.5f * (1 + Mathf.Sin(time - Mathf.PI * 0.5f))) * Size;
		gameObject.transform.localScale = new Vector3(modifier, modifier, modifier);

		Debug.Log(modifier);
		if( time >= Duration )
		{
			time = 0.0f;
			Active = false;
			gameObject.transform.localScale = new Vector3(1,1,1);
			if(sprRnd != null)
			{
				sprRnd.enabled = false;
				sprRnd.material.SetFloat("_TimePassed", time);
			}
		}
	}
}

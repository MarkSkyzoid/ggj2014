﻿using UnityEngine;
using System.Collections;

public class PulseController : MonoBehaviour {
    public GameObject pulse;
    public static bool pulsing = false;
    bool sendPulse = false;
    float radius = 0;
    public float maxRadius = 4;
    public float pulseTime = 5;
    public float pulseSpeed = 7;
    Vector2 pos;


	// Use this for initialization
	void Start () {
        pos = Vector2.zero;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.F) && !sendPulse)
        {
            pos = transform.position;
            pulse.transform.position = pos;
            StartCoroutine(SendPulse());
        }

        if (sendPulse && radius < maxRadius)
            radius += pulseSpeed * Time.deltaTime;
        else if (!sendPulse && radius >= 0)
            radius -= pulseSpeed * 4 * Time.deltaTime;
        
        if (!sendPulse && radius <= 0 && pulse.renderer.enabled) 
            pulse.renderer.enabled = false;

        pulse.transform.localScale = new Vector3(radius / maxRadius, radius / maxRadius, radius / maxRadius);

        if (radius < 0)
            radius = 0;
        Debug.DrawLine(pos, pos + (Vector2.right * radius));
        Debug.DrawLine(pos, pos + (-Vector2.right * radius));
        Debug.DrawLine(pos, pos + (-Vector2.up * radius));
        Debug.DrawLine(pos, pos + (Vector2.up * radius));


        //Handle pulse collisions
        if (sendPulse)
        {
            Collider2D[] hitColliders = Physics2D.OverlapCircleAll(pos, radius);

            foreach (Collider2D col in hitColliders)
            {
                PulseReceiver r = col.GetComponent<PulseReceiver>();
                if (r != null)
                {
                    if (!r.pulseHit)
                        r.PulseEnter();
                }
            }
        }
	}

    IEnumerator SendPulse()
    {
        sendPulse = true;
        pulse.renderer.enabled = true;
        PulseController.pulsing = true;
        yield return new WaitForSeconds(pulseTime);
        PulseController.pulsing = false;
        sendPulse = false;
    }
}

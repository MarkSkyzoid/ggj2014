using System;
using UnityEngine;

public enum AIStates
{
	IDLE,
	PATROLLING,
	ATTACKING,
	FOLLOWING
};

public abstract class AI :  MonoBehaviour
{

	protected AIStates CurrentState = AIStates.IDLE, NextState = AIStates.IDLE;
	public GameObject Target;

	void Update()
	{
		switch (CurrentState)
		{
		case AIStates.IDLE:
			IdleCode();
			break;
		case AIStates.FOLLOWING:
			FollowCode();
			break;
		case AIStates.PATROLLING:
			PatrolCode();
			break;
		case AIStates.ATTACKING:
			AttackCode();
			break;
		}

		UpdateState ();

	}

	private void UpdateState()
	{
		if (CurrentState != NextState)
			CurrentState = NextState;
	}

	protected abstract void IdleCode();
	protected abstract void FollowCode();
	protected abstract void PatrolCode();
	protected abstract void AttackCode();

	public abstract void HasPulsed();
	
}

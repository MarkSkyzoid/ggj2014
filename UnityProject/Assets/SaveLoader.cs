using UnityEngine;    // For Debug.Log, etc.

using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

using System;
using System.Runtime.Serialization;
using System.Reflection;

[AddComponentMenu ("CustomScripts/Persistance/SaveLoad")]
public class SaveLoad {
	
	public static string currentSaveLocation = "SaveData.sav";    // Edit this for different save files
	
	// Call this to write data
	public static void Save (SaveGameData data) { Save (currentSaveLocation, data); }
	public static void Save (string filePath, SaveGameData data)
	{
		Stream stream = File.Open(filePath, FileMode.Create);
		BinaryFormatter bformatter = new BinaryFormatter();
		bformatter.Binder = new VersionDeserializationBinder(); 
		bformatter.Serialize(stream, data);
		stream.Close();
	}
	
	// Call this to load from a file into "data"
	public static void Load (ref SaveGameData data)  { Load(currentSaveLocation, ref data);  }   
	public static void Load (string filePath, ref SaveGameData data) 
	{
		Stream stream = File.Open(filePath, FileMode.Open);
		BinaryFormatter bformatter = new BinaryFormatter();
		bformatter.Binder = new VersionDeserializationBinder(); 
		data = (SaveGameData)bformatter.Deserialize(stream);
		stream.Close();
		
		// Now use "data" to access your Values
	}
	
}


[Serializable ()]
public class SaveGameData : ISerializable {
	
	public float PositionX, PositionY;
	public string Level;
	
	public SaveGameData () {}

	public void Initialise(float x, float y, string lvl  )
	{
		PositionX = x;
		PositionY = y;
		Level = lvl;
	}
	
	public SaveGameData (SerializationInfo info, StreamingContext ctxt)
	{
		// Get the values from info and assign them to the appropriate properties. Make sure to cast each variable.
		// Do this for each var defined in the Values section above
		PositionX = (int)info.GetValue("PositionX", typeof(int));
		PositionY = (int)info.GetValue("PositionY", typeof(int));
		Level = (string)info.GetValue("Level", typeof(string));
	}
	
	// Called automatically
	public void GetObjectData (SerializationInfo info, StreamingContext ctxt)
	{
		// Repeat this for each var defined in the Values section
		info.AddValue("PositionX", (PositionX));
		info.AddValue("PositionY", (PositionY));
		info.AddValue("Level", Level);
	}
}

public sealed class VersionDeserializationBinder : SerializationBinder 
{ 
	public override Type BindToType( string assemblyName, string typeName )
	{ 
		if ( !string.IsNullOrEmpty( assemblyName ) && !string.IsNullOrEmpty( typeName ) ) 
		{ 
			Type typeToDeserialize = null; 
			
			assemblyName = Assembly.GetExecutingAssembly().FullName; 
			
			// The following line of code returns the type. 
			typeToDeserialize = Type.GetType( String.Format( "{0}, {1}", typeName, assemblyName ) ); 
			
			return typeToDeserialize; 
		} 
		
		return null; 
	} 
}